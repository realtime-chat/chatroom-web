export default {
    ssr: false,
    head: {
        title: 'chat-room',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    styleResources: {
        scss: [
            '~styles/vars/*.scss',
        ],
    },
    router: {
        linkExactActiveClass: 'active-link',
    },
    css: [
        'ant-design-vue/dist/antd.css',
        'toastr/build/toastr.min.css',
    ],
    plugins: [
        '@/plugins/antd-ui',
        { src: '~/plugins/persist', ssr: false },
    ],
    components: true,
    buildModules: [
        '@nuxtjs/eslint-module'
    ],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/style-resources',
    ],
    axios: {},
    build: {
    }
}
