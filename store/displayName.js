export const state = () => ({
    displayName: '',
})

export const mutations = {
    setDisplayName(state, displayName) {
        state.displayName = displayName
    },
    resetDisplayName(state) {
        state.displayName = ''
    },
}
