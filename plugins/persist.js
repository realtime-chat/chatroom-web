import VuexPersistence from 'vuex-persist'

export default ({ store }) => {
    new VuexPersistence({
        key: 'chatroom',
        reducer: (state) => ({
            displayName: state.displayName,
        }),
    }).plugin(store)
}
